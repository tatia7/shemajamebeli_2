package com.example.shemajamebeli2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var addButton: Button
    private lateinit var removeButton:  Button
    private lateinit var updateButton: Button
    private lateinit var firstNameEditText: EditText
    private lateinit var lastNameEditText: EditText
    private lateinit var ageEditText: EditText
    private lateinit var emailEditText: EditText
    private lateinit var lstofUser: MutableList<String>
    private lateinit var active: TextView
    private lateinit var deletedUsers: MutableList<String>
    private lateinit var inactive: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        lstofUser = mutableListOf()
        deletedUsers = mutableListOf()
        addButton.setOnClickListener {
            addUser()
        }
        removeButton.setOnClickListener {
            removeUser()
        }
        updateButton.setOnClickListener {
            updateUser()
        }
    }
    private fun init(){
        addButton = findViewById(R.id.add_user)
        removeButton = findViewById(R.id.remove)
        updateButton = findViewById(R.id.update)
        firstNameEditText = findViewById(R.id.firstname)
        lastNameEditText = findViewById(R.id.lastname)
        ageEditText = findViewById(R.id.age)
        emailEditText = findViewById(R.id.email)
        active = findViewById(R.id.active)
        inactive = findViewById(R.id.inactive)
    }
    private fun addUser() {
        var email : String =  emailEditText.text.toString()
        if (email.isNotEmpty() && email !in lstofUser) {
            lstofUser.add(email)
            Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
        }
        else if (email.isEmpty()){
            Toast.makeText(this, "Adding user failed", Toast.LENGTH_SHORT).show()
        }
        else if (email in lstofUser){
            Toast.makeText(this, "User already Exists", Toast.LENGTH_SHORT).show()
        }
    }
    private fun removeUser() {
        var email : String =  emailEditText.text.toString()
        if (email in lstofUser) {
            lstofUser.remove(email)
            deletedUsers.add(email)
            Toast.makeText(this, "User deleted successfully", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "User doesn't Exists", Toast.LENGTH_SHORT).show()
        }
    }
    private fun updateUser() {
        var email : String =  emailEditText.text.toString()
        if (email !in lstofUser) {
            Toast.makeText(this, "Doesn't Exist", Toast.LENGTH_SHORT).show()
        }else{
            lstofUser = mutableListOf()
            lstofUser.add(email)
            Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show()
        }
    }
}